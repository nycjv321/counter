package client

import (
	"encoding/json"
	"gitlab.com/nycjv321/counter/model"
	"io/ioutil"
	"net/http"
)

type Client struct {
	BaseUrl string
}

func CreateClient(url string) (*Client) {
	return &Client{url}
}

func (c Client) GetCounter() (*model.Counter, error) {

	if resp, err := http.Get(c.BaseUrl + "/counter"); err == nil {

		defer func() {
			if err = resp.Body.Close(); err != nil {
				panic(err)
			}
		}()
		body, err := ioutil.ReadAll(resp.Body)
		count := model.Counter{}
		if err = json.Unmarshal(body, &count); err == nil {
			return &count, nil
		} else {
			return nil, err
		}
	} else {
		return nil, err
	}

}
