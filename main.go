package main

import (
	"fmt"
	config "gitlab.com/nycjv321/counter/configuration"
)

func main() {
	panic(initializeEngine().Run(getAddress()))
}

func getAddress() string {
	return fmt.Sprintf(":%v", config.GetApplicationConfiguration().Port)
}
