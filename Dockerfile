FROM alpine:3.9
LABEL maintainer="Javier L. Velasquez <nycjv321@gmail.com>"

ENV DATABASE_MIGRATION_PATH /opt/counter/migrations
ARG CI_COMMIT_REF_NAME
ENV CI_COMMIT_REF_NAME=${CI_COMMIT_REF_NAME:-"not provided"}
ARG CI_COMMIT_SHA
ENV CI_COMMIT_SHA=${CI_COMMIT_SHA:-"not provided"}
ARG APPLICATION_VERSION
ENV APPLICATION_VERSION=${APPLICATION_VERSION:-"not provided"}

WORKDIR /opt/counter

COPY counter /opt/counter/counter
COPY migrations /opt/counter/migrations
COPY index.html /opt/counter

EXPOSE 8080

CMD ["/opt/counter/counter"]

