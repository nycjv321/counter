package service

import (
	"database/sql"
	"errors"
	"fmt"
	_ "github.com/lib/pq"
	_ "gitlab.com/nycjv321/digme"
	config "gitlab.com/nycjv321/counter/configuration"

)

func createConnection() *sql.DB {
	c := config.GetDatabaseConfiguration()
	connStr := fmt.Sprintf("postgres://%s:%s@%s/%s?sslmode=disable",
		c.Username, c.Password, c.Host, c.Name)
	if db, err := sql.Open("postgres", connStr); err != nil {
		panic(err)
	} else {
		if db.Ping() != nil {
			panic("unable to connect to db")
		}
		return db
	}
}

var db = createConnection()

func Get() (int, error) {
	if rows, err := db.Query("SELECT nextval('counter');"); err == nil {
		if rows.Next() {
			var count int
			if err = rows.Scan(&count); err != nil {
				return 0, nil
			} else {
				return count, nil
			}
		} else {
			return 0, errors.New("unable to resolve query")
		}
	} else {
		return 0, err
	}

}
