package info

import (
	"fmt"
	"os"
)

func GetCommitRefName() string {
	return getEnvironmentVariableNoDefault("CI_COMMIT_REF_NAME")
}


func GetCommitSha() string {
	return getEnvironmentVariableNoDefault("CI_COMMIT_SHA")
}

func GetApplicationVersion() string {
	return getEnvironmentVariableNoDefault("APPLICATION_VERSION")
}

func getEnvironmentVariableNoDefault(name string) string {
	return getEnvironmentVariable(name, fmt.Sprintf("not provided"))
}

func getEnvironmentVariable(name, defaultValue string) string {
	resolvedValue := os.Getenv(name)
	if len(resolvedValue) == 0 {
		return defaultValue
	} else {
		return resolvedValue
	}
}