package configuration

import 	"github.com/kelseyhightower/envconfig"

type ApplicationConfiguration struct {
	Port int `default:"8080"`
}

type DatabaseConfiguration struct {
	Name     string `default:"postgres"`
	Host     string `default:"localhost"`
	Username string `default:"postgres"`
	Password string `default:"postgres"`
}

func GetDatabaseConfiguration() *DatabaseConfiguration {
	var c DatabaseConfiguration

	if err := envconfig.Process("database", &c); err != nil {
		panic(err)
	}
	return &c
}

func GetApplicationConfiguration() *ApplicationConfiguration {
	var c ApplicationConfiguration

	if err := envconfig.Process("counter", &c); err != nil {
		panic(err)
	}
	return &c
}