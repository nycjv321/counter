package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/nycjv321/counter/model"
	counter "gitlab.com/nycjv321/counter/service"
	"gitlab.com/nycjv321/counter/service/info"
	"net/http"
)

func initializeEngine() *gin.Engine {
	r := gin.Default()

	r.LoadHTMLFiles("index.html")

	r.GET("/", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.html", gin.H{})
	})

	r.OPTIONS("/counter", func(c *gin.Context) {
		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Method", "OPTIONS, GET")
		c.JSON(http.StatusOK, gin.H{})
	})

	r.GET("/counter", func(c *gin.Context) {
		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Method", "OPTIONS, GET")
		if count, err := counter.Get(); err == nil {
			c.JSON(http.StatusOK, model.Counter{count})
		} else {
			c.JSON(http.StatusInternalServerError, gin.H{
				"message": err,
			})
		}
	})

	r.GET("/info", func(c *gin.Context) {
		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Method", "OPTIONS, GET")
		c.JSON(http.StatusInternalServerError, gin.H{
			"commit_ref_name":     info.GetCommitRefName(),
			"commit_sha":          info.GetCommitSha(),
			"application_version": info.GetApplicationVersion(),
		})
	})

	return r
}
