
# Counter Service

This is a pretty simple counter app used for demonstration purposes

## Spinning up an auxillary Postgres Database

    docker run --name counter-database -p 5432:5432 -d postgres

## Docker

### Minikube
Run the command below: if your planning on pushing/pulling to/from minikube

    eval $(minikube docker-env)

### Building

    docker build -t counter-service .

### Running 
    
    docker run -d --name counter-service counter-service
    
### A local docker repository    

    docker run -d -p 5000:5000 --restart=always --name registry registry:2

### Tagging
    docker tag counter-service localhost:5000/counter-service

### Pushing
    docker push localhost:5000/counter-service
    
## Kubernetes

### kubectl

    curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
    chmod +x ./kubectl
    sudo mv ./kubectl /usr/local/bin/kubectl
    
#### Minikube

##### Installing
    curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 \
    && chmod +x minikube
    sudo install minikube /usr/local/bin
    
    curl -LO https://storage.googleapis.com/minikube/releases/latest/docker-machine-driver-kvm2 \
    && sudo install docker-machine-driver-kvm2 /usr/local/bin/


##### Running 

    minikube start
    
    kubectl config use-context minikube
    
    minikube dashboard


##### Checking your connection

    $ kubectl get nodes
    NAME       STATUS   ROLES    AGE   VERSION
    minikube   Ready    <none>   45s   v1.15.0 # you're looking for "Ready"

    
##### Deleting Local State

    minikube delete
        
### Helm

### Initialization

    helm init
    
### Updating Dependencies
    
    helm repo update

### Fetching Dependencies    
    
    helm dependencies update counter-chart

#### Packaging

    helm package counter-chart/

#### Deploying

##### Using Local/Development Configuration

    helm upgrade --install test counter-chart -f counter-chart/local.yaml 

##### Porting Forwarding

    kubectl port-forward deployment/counter-service 8080:8080

## CI/CD Tooling

### Longshoreman

    https://gitlab.com/chili_con_carne/cicd/longshoreman
    
### skipper    

    https://gitlab.com/chili_con_carne/cicd/skipper