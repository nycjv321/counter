package main

import (
	"gitlab.com/nycjv321/counter/client"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	_ "gitlab.com/nycjv321/counter/client"
)

func TestPingRoute(t *testing.T) {
	server := httptest.NewServer(initializeEngine())

	counterClient := client.CreateClient(server.URL)
	counter, err := counterClient.GetCounter()
	assert.Nil(t, err)
	assert.True(t, counter.Count > 0, "Expected for counter to return data")
}
